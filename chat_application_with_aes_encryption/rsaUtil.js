const fs = require('fs');
const crypto = require('crypto');
require('dotenv').config();
// Base64 decode the symmetric key and IV
const symmetricKeyAes = Buffer.from(process.env.RSA_AES_KEY, 'base64');
const ivAes = Buffer.from(process.env.RSA_AES_IV, 'base64');

class Rsa {
    constructor(KeyPath) {
        this.algorithm = 'aes-256-cbc';
        this.KeyPath = KeyPath;
        this.symmetricKey = symmetricKeyAes;
        this.iv = ivAes;
    }

    // Method to encrypt data
    encryptData(text) {
        const publicKey = fs.readFileSync('./public_key.pem', 'utf8'); // Public key
        let encryptedMessage = crypto.publicEncrypt(publicKey, Buffer.from(text));
        encryptedMessage=encryptedMessage.toString('base64');
        const cipher = crypto.createCipheriv(this.algorithm, this.symmetricKey, this.iv);
        let encrypted = cipher.update(encryptedMessage, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return encrypted;
    }

    // Method to decrypt data
    decryptData(encryptedText) {
        const decipher = crypto.createDecipheriv(this.algorithm, this.symmetricKey, this.iv);
        let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        const privateKey = fs.readFileSync('./private_key.pem', 'utf8'); // Private key
        const decryptedMessage = crypto.privateDecrypt(privateKey, Buffer.from(decrypted, 'base64'));
        return decryptedMessage.toString()
    }
}

module.exports = Rsa;