const net = require('net');
const crypto = require('crypto');
require('dotenv').config(); 
const {CHANNEL_OPRATOR,ENCRYPTION_TYPE} = require('./constants');
const readline = require('readline');
const logger = require('./logger'); 
const PORT = process.env.PORT;
const Rsa = require('./rsaUtil');
const Aes = require('./aesUtil');

class ChatClient {
  constructor(port,publicKeyPath) {
    this.port = port;
    this.key = crypto.randomBytes(32); // Generate a random key
    this.iv = crypto.randomBytes(16);// Generate a random IV (Initialization Vector)
    this.client = null;
    this.rl = null;
    this.userName = '';
    this.connectToServer();
    this.clientInputHandler();
    this.rsa = new Rsa(publicKeyPath);
    this.aes = new Aes();
  }

  connectToServer() {
    this.client = net.createConnection({ port: this.port }, () => {
      logger.info('CLIENT: I connected to the server.');
      this.client.write(JSON.stringify({
        encryptionType:ENCRYPTION_TYPE.RSA,
        secretInfo:this.rsa.encryptData(JSON.stringify({
          key:this.key.toString('base64'),
          iv:this.iv.toString('base64')
        }))
      }));
    });

    this.client.on('data', (data) => {
      data=this.aes.decryptData(data.toString(),this.key,this.iv)
      this.serverData(data);
    });

    this.client.on('end', () => {
      this.handleServerEnd();
    });
  }

  serverData(data) {
    const parsedData = JSON.parse(data.toString());
    logger.info(`Data received from server: ${JSON.stringify(parsedData)}`);
    this.setUserName(parsedData);
  }

  setUserName(data) {
    if (data?.op === 'change-name' && data?.userName) {
      this.userName = data?.userName;
      logger.info(`User name set to: ${this.userName}`);
    }
  }

  handleServerEnd() {
    logger.info('CLIENT: I disconnected from the server.');
  }

  clientInputHandler() {
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    this.rl.on('line', (userInput) => {
      try {
        const payload = JSON.parse(userInput);
        this.sendMessageToServer({
          ...payload,
          userName: payload?.op !== CHANNEL_OPRATOR.CHANGE_NAME ? this.userName : payload?.userName
        });
      } catch (e) {
        logger.error(`Invalid user input: ${e.message}`);
        logger.info('Invalid user input format. Please provide input in JSON format: {"message":"your message", "op":"change-name/@"}, userName: "required when user name is changed."');
      }
    });
  }

  sendMessageToServer(payload) {
    this.client.write(JSON.stringify({
      encryptionType:ENCRYPTION_TYPE.AES,
      encryptedData:this.aes.encryptData(JSON.stringify(payload),this.key,this.iv)
    }));
    logger.info(`Message sent to server: ${JSON.stringify(payload)}`);
  }
}

new ChatClient(PORT,process.env.CLIENT_PUBLIC_KEY);
