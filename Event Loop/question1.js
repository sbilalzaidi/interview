/**
 * Promise and nextTick are in micro task queue
 * Preceedence nextTick is higher than promise
 * Execution order
 * micro[1]
 * micro[2]
 */
Promise.resolve().then(()=>{
    console.log('micro[2]')
})

process.nextTick(()=>{
    console.log('micro[1]')
})