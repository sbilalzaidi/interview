/**
 * Event loop has 6 phases
 * timers: this phase executes callbacks scheduled by setTimeout() and setInterval().
 * pending callbacks: executes I/O callbacks deferred to the next loop iteration.
 * idle, prepare: only used internally.
 * poll: retrieve new I/O events; execute I/O related callbacks (almost all with the exception of close callbacks, the ones scheduled by timers, and setImmediate()); node will block here when appropriate.
 * check: setImmediate() callbacks are invoked here.
 * close callbacks: some close callbacks, e.g. socket.on('close', ...).
 */
/**
 * Note :Micro queue has higher precident than macro queue
 * Micro:promise ,process next tick
 * Macro:setTimeout and setInterval
 * Execution order
 * print stack 1
 * print stack 8
 * micro[7] is printed 2 times because is micro task
 * then print macro[2] and macro[3]
 * print macro task macro[4] and print micro task micro[6] repeated 2 time 
 * then print macro task marco[5] 2 time
 */
console.log("stack[1]")
setTimeout(()=>console.log("macro[2]"),0)
setTimeout(()=>console.log("macro[3]"),0)
const p=Promise.resolve();
for(let i=0;i<2;i++){
    p.then(()=>{
        setTimeout(()=>{
            console.log("macro[4]") 
            setTimeout(()=>console.log("macro[5]"),0)
            p.then(()=>console.log("micro[6]"))
        },0)
        console.log("micro[7]")
    })
}
console.log("stack[8]")

