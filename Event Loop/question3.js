console.log("Start of the script");

// Phase 1: Execute Synchronous Tasks
console.log("Synchronous Task 1");

// Phase 2: Execute Timer (setTimeout) Callback
setTimeout(() => {
  console.log("Timer Callback (Phase 2)");
}, 0);

// Phase 3: Execute Microtasks (Promise)
Promise.resolve().then(() => {
  console.log("Microtask (Phase 3)");
});

// Phase 4: Execute I/O Callback (not a real I/O operation in this example)
const fakeIOOperation = () => {
  console.log("I/O Callback (Phase 4)");
};

// Simulating an asynchronous I/O operation
setTimeout(fakeIOOperation, 200);

// Phase 5: Execute Synchronous Task 2
console.log("Synchronous Task 2");

console.log("End of the script");
