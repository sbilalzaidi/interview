/**
 * https://stackoverflow.com/questions/77736974/
 * precedence-of-setimmediate-and-settimeout-callbacks-
 * in-commonjs-vs-esm
 
Calling setTimeout with setImmediate has unpredicted behaviour
if using CommonJS modules. But if you switch to 
ESM ("type": "module" in package.json), it will always
execute setImmediate before setTimeout.
 
In a CommonJS environment, such as Node.js, the execution order 
between setTimeout and setImmediate can be unpredictable when 
called in quick succession with a very short delay (like 0 milliseconds).
 This is due to the way Node.js handles CommonJS modules and 
 their synchronous loading.

With ESM (ECMAScript Modules) and the 
"type": "module" option set in package.json, 
modules are loaded asynchronously and follow a more 
standardized event handling model. In this case, 
because modules are loaded asynchronously, 
setImmediate is more likely to be executed before 
setTimeout, even with a delay of 0 milliseconds.

One possible solution to ensure the desired execution
order is to use a code structure that doesn't rely on the order 
of module loading. For instance, you can use setImmediate or 
setTimeout with a slightly larger delay than 0 
milliseconds to ensure the desired order:
 */
setTimeout(() => {
    console.log('timeout');
  }, 0);
  
setImmediate(() => {
  console.log('immediate');
});