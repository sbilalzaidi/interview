async function tellJoke() {
    try {
        const joke = await fetchRandomJoke();
        console.log(joke);

        const translatedJoke = await translateJoke(joke);
        console.log(translatedJoke);

        await postJoke(translatedJoke);
        console.log("Joke Translated & posted successfully!");
    } catch (error) {
        console.error("Something went wrong:", error);
    }
};

tellJoke();
/**
 * Async functions enable writing asynchronous 
 * code that looks and behaves like synchronous code.
 */
async function fetchData() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('Data fetched successfully');
      }, 2000);
    });
  }
  
  async function processData() {
    try {
      const data = await fetchData();
      console.log(data);
    } catch (error) {
      console.error(error);
    }
  }
  
  processData();
  