/**
 * Generators allow pausing and resuming the execution of a function,
 *  enabling more complex control flow.
 */
function* generatorFunction() {
    yield 1;
    yield 2;
    yield 3;
  }
  
  const generator = generatorFunction();
  
  console.log(generator.next().value); // 1
  console.log(generator.next().value); // 2
  console.log(generator.next().value); // 3

  /**
   * Fibnacci 
   */
  function* fibonacciGenerator(limit) {
    let prev = 0;
    let curr = 1;
    while (curr <= limit) {
      yield curr;
      [prev, curr] = [curr, prev + curr];
    }
  }
  
  const fibonacci = fibonacciGenerator(50); // Generating Fibonacci numbers up to 50
  
  for (const num of fibonacci) {
    console.log(num);
  }
    