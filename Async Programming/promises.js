fetchRandomJoke()
    .then((joke) => {
        console.log(joke);
        return translateJoke(joke);
    })
    .then((translatedJoke) => {
        console.log(translatedJoke);
        return postJoke(translatedJoke);
    })
    .then(() => {
        console.log("Joke posted successfully!");
    })
    .catch((error) => {
        console.error("Something went wrong:", error);
    });

/**
 * Promises represent the eventual completion or failure of an 
 * asynchronous operation, and its resulting value.
 */
function fetchData() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('Data fetched successfully');
      }, 2000);
    });
  }
  
  fetchData()
    .then(data => console.log(data))
    .catch(error => console.error(error));
    const promise1 = new Promise((resolve) => setTimeout(() => resolve('Promise 1 resolved'), 1000));
    const promise2 = new Promise((resolve, reject) => setTimeout(() => reject('Promise 2 rejected'), 500));
    const promise3 = new Promise((resolve) => setTimeout(() => resolve('Promise 3 resolved'), 1500));
    
    Promise.all([promise1, promise2])
      .then((results) => {
        console.log('All promises resolved:', results);
      })
      .catch((error) => {
        console.error('At least one promise rejected:', error);
      });
    
    
    Promise.race([promise1, promise2])
      .then((results) => {
        console.log('All promises resolved:', results);
      })
      .catch((error) => {
        console.error('At least one promise rejected:', error);
      });
    
    
    Promise.allSettled([promise1, promise2, promise3])
      .then((results) => {
        console.log('All promises settled:', results);
      });
    