//Callback Hell
fetchRandomJoke((joke) => {
    console.log(joke);

    translateJoke(joke, (translatedJoke) => {
        console.log(translatedJoke);

        postJoke(translatedJoke, () => {
            console.log("Joke posted successfully!");
        });
    });
});
/**
 * Callbacks are functions passed as arguments 
 * to another function to be executed later, usually asynchronously.
 */
function fetchData(callback) {
    setTimeout(() => {
      callback('Data fetched successfully');
    }, 2000);
  }
  
  function processData(data) {
    console.log(data);
  }
  
  fetchData(processData);
  