export default interface IFactorialCalculator {
  calculateFactorial(n: number): any;
}