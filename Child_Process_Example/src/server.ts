import { fork } from 'child_process';
import * as http from 'http';
import * as path from 'path';
import * as url from 'url';
const PORT = 3000;

// Fork the child process
const child = fork(path.join(__dirname, 'ProcessFactorialWorker.js')); // Ensure the correct path

const server = http.createServer((req:any, res) => {
    const query:any = url.parse(req.url, true).query;
    const number:number = parseInt(query.number, 10);
    const op:string=query.op;
    if (isNaN(number)) {
      res.writeHead(400, { 'Content-Type': 'text/plain' });
      res.end('Invalid number');
      return;
    }
    // Send message to child process
    switch(op){
        case 'factoriral':
            child.send({ op: 'factoriral', data: { number : number}});
            break;
        default:
            res.writeHead(400, { 'Content-Type': 'text/plain' });
            return res.end('Please select valid oprator');
    }
    

    // Listen for message from child process
    child.once('message', (message: string) => {
        console.log(`Message from child: ${message}`);
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end(`${op} is:  ${message}`);
    });
    child.once('err',(err: any, data: any) => { 
        res.writeHead(400, { 'Content-Type': 'text/plain' });
        res.end('Error',err);
     })
});

server.listen(PORT, () => {
    console.log(`Server running at http://localhost:${PORT}/`);
});
