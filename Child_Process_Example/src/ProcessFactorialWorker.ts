import FactorialCalculator  from './factorialCalculator';
class ProcessFactorialWorker extends FactorialCalculator {
    constructor(process:any) {
        super();
        // Listen for messages from the parent process
        process.on('message', (message: any) => {
            let result:string|number='';
            switch(message.op){
                case 'factoriral':
                    result = this.calculateFactorial(message.data.number);
                    process.send(result.toString());
                    break;
                default:
                    process.send('Please send oprator');
                    break;
            }
        });
    }
}

// Instantiate the worker class
new ProcessFactorialWorker(process);
