import IFactorialCalculator from './IFactorialCalculator.js';

class FactorialCalculator implements IFactorialCalculator {
  private static factorialCalculatorInstance: FactorialCalculator;
  public static getInstance(): FactorialCalculator {
    if (!this.factorialCalculatorInstance) {
      this.factorialCalculatorInstance = new FactorialCalculator();
    }
    return this.factorialCalculatorInstance;
  }
  calculateFactorial(n: number):any{
    if (n < 0) throw new Error('Negative numbers are not allowed.');
    if (n === 0 || n === 1) return 1;
    return n * this.calculateFactorial(n - 1);
  }
}

export default FactorialCalculator;