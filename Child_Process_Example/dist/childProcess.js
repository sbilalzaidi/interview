"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const factorialCalculator_1 = __importDefault(require("./factorialCalculator"));
class ProcessFactorialWorker extends factorialCalculator_1.default {
    constructor(process) {
        super();
        // Listen for messages from the parent process
        process.on('message', (message) => {
            if (message.start === 'Start calculation') {
                const result = this.calculateFactorial(message.number);
                process.send(result.toString());
            }
        });
    }
}
// Instantiate the worker class
new ProcessFactorialWorker(process);
