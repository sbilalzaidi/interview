"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class FactorialCalculator {
    static getInstance() {
        if (!this.factorialCalculatorInstance) {
            this.factorialCalculatorInstance = new FactorialCalculator();
        }
        return this.factorialCalculatorInstance;
    }
    calculateFactorial(n) {
        if (n < 0)
            throw new Error('Negative numbers are not allowed.');
        if (n === 0 || n === 1)
            return 1;
        return n * this.calculateFactorial(n - 1);
    }
}
exports.default = FactorialCalculator;
