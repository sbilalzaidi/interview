"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const factorialCalculator_1 = __importDefault(require("./factorialCalculator"));
class ProcessFactorialWorker extends factorialCalculator_1.default {
    constructor(process) {
        super();
        // Listen for messages from the parent process
        process.on('message', (message) => {
            let result = '';
            switch (message.op) {
                case 'factoriral':
                    result = this.calculateFactorial(message.data.number);
                    process.send(result.toString());
                    break;
                default:
                    process.send('Please send oprator');
                    break;
            }
        });
    }
}
// Instantiate the worker class
new ProcessFactorialWorker(process);
