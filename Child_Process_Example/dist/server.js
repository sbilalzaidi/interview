"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const http = __importStar(require("http"));
const path = __importStar(require("path"));
const url = __importStar(require("url"));
const PORT = 3000;
// Fork the child process
const child = (0, child_process_1.fork)(path.join(__dirname, 'ProcessFactorialWorker.js')); // Ensure the correct path
const server = http.createServer((req, res) => {
    const query = url.parse(req.url, true).query;
    const number = parseInt(query.number, 10);
    const op = query.op;
    if (isNaN(number)) {
        res.writeHead(400, { 'Content-Type': 'text/plain' });
        res.end('Invalid number');
        return;
    }
    // Send message to child process
    switch (op) {
        case 'factoriral':
            child.send({ op: 'factoriral', data: { number: number } });
            break;
        default:
            res.writeHead(400, { 'Content-Type': 'text/plain' });
            return res.end('Please select valid oprator');
    }
    // Listen for message from child process
    child.once('message', (message) => {
        console.log(`Message from child: ${message}`);
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end(`${op} is:!\n ${message}`);
    });
    child.once('err', (err, data) => {
        res.writeHead(400, { 'Content-Type': 'text/plain' });
        res.end('Error', err);
    });
});
server.listen(PORT, () => {
    console.log(`Server running at http://localhost:${PORT}/`);
});
