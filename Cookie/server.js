const express = require('express');
const cookieParser = require('cookie-parser');

const app = express();
app.use(cookieParser());

app.get('/set-cookie', (req, res) => {
  // Set a cookie
  res.cookie('myCookie', 'value', { secure: true, httpOnly: true });
  res.send('Cookie set successfully!');
});

app.get('/get-cookie', (req, res) => {
  // Retrieve a cookie
  const myCookie = req.cookies.myCookie;
  res.send(`Cookie value: ${myCookie}`);
});

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
