Cluster
Worker thread
Event Loop Phase (With Microtask Queue)
NodeJS Stream
Create HTTP api with generic status code

owasp top 10
CSP(Content Security Policy helmet)
XSS (Cross-Site Scripting xss) 
CSRF(Cross-Site Request Forgery curf)
CORS(Cross-Origin Resource Sharing cores)
Cookies with http and non http
HSTS(HTTP Strict Transport Security)
LTS(Long-Term Support) 
encryption module
Authentication
Authorisation
Oauth
openid
jwt

SOLID design principle
Design patterns
Basic Typescript (like interface, types, generics, etc..)
sql ( aggregate function (sum, avg, etc..), all window function( dense rank, rank, row number, coalesce, etc..), all joins, sql ordering, function, view, store procedure, pagination using sql query, group, having, general query and subquery)
 
learn bellow topic
AWS Lambda
AWS API gateway
AWS Cloud Formation
AWS Http vs rest api gateway

function deepCopy(obj) {
  if (typeof obj !== 'object' || obj === null) {
    return obj; // base case: return non-object values or null
  }
  const newObj = Array.isArray(obj) ? [] : {};
  for (let key in obj) {
    newObj[key] = deepCopy(obj[key]); // recursively copy nested objects
  }
  return newObj;
}

const original = { a: 1, b: { c: 2 } };
const deepCopy = deepCopy(original);
