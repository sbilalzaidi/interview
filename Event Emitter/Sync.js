class EventEmitter {
    constructor() {
      this.events = {};
    }
  
    on(eventName, callback) {
      if (!this.events[eventName]) {
        this.events[eventName] = [];
      }
      this.events[eventName].push(callback);
    }
  
    emit(eventName, ...args) {
      if (this.events[eventName]) {
        this.events[eventName].forEach(callback => {
          callback(...args);
        });
      }
    }
  
    off(eventName, callback) {
      if (this.events[eventName]) {
        this.events[eventName] = this.events[eventName].filter(cb => cb !== callback);
      }
    }
  }
  
  // Example usage:
  const emitter = new EventEmitter();
  
  // Subscribe to an event
  emitter.on('message', (message) => {
    console.log('Received message:', message);
  });
  
  // Emit an event
  emitter.emit('message', 'Hello, world!');
  
  // Unsubscribe from an event
  const callback = (message) => {
    console.log('Another message received:', message);
  };
  emitter.on('message', callback);
  emitter.off('message', callback);
  
  // Emit the event again
  emitter.emit('message', 'Another hello!');
  