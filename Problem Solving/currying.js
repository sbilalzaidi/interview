function sumOfNumbers(a,b,c){
    return a+b+c;
}
//convert currying function
 const sum=(a)=>(b)=>(c)=>a+b+c;

 console.log(sum(1)(2)(3))