//program-1: addition of the numbers present in the string
const str = 'AB_02_CD_203_EF_20';
//o/p = 12+23+20 = 55

function sumOfDigit(str){
    let currentNo=0;
    let sum=0;
    for(let i=0;i<str.length;i++){
        if(str[i]>=0 && str[i]<=9){
            currentNo =currentNo *10+(+str[i]);
        }else if (currentNo !== 0) {
            sum += currentNo;
            currentNo = 0;
          }
    }
    if (currentNo !== 0) {
        sum += currentNo;
      }
      return sum
}

console.log(sumOfDigit(str))