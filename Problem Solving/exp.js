function fabonacci(n){
    const fab=[0,1];
    for(let i=2;i<n;i++){
        fab[i]=fab[i-1]+fab[i-2];
    }
    return fab;
}
console.log(fabonacci(6))
//Big-o 2^n
function recursionSumFabonacci(n){
    if(n<2)
    return n;
    return recursionSumFabonacci(n-1)+recursionSumFabonacci(n-2);
}
function factorial(n){
    let result=1;
    for(let i=2;i<=n;i++){
        result=result*i;
    }
    return result;
}

console.log(factorial(5))
//Big-o O(n)
function recursionFactorial(n){
    if(n==0)
    return 1;
    return n*recursionFactorial(n-1);
}
console.log(recursionFactorial(5))
function isPrime(n){
    if(n<2)
    return false;
    for(let i=2;i<n;i++){
        if(n%i==0)
        return false;
    }
    return true;
}
console.log(isPrime(5))

function isPowerOfTwo(n){
    if(n<1)
    return false;
    while(n>1){
        if(n%2 !=0)
        return false
        n=n/2;
    }
    return true;
}

console.log(isPowerOfTwo(5))
// bitwise 1+1 =1 otherwise 0
function isPowerOfTwoBitwise(n){
    if(n<1)
    return false;
    return (n & (n-1))==0
}
console.log(isPowerOfTwoBitwise(1))
//Big-o o(n)
function linearSearch(arr,n){
    for(let i=0;i<n;i++){
        if(arr[i]==n)
        return i
    }
    return -1;
}
console.log(linearSearch([3,5,6],5))
function binarySearch(arr,target){
    arr=arr.sort((a,b)=>a-b);
    let leftIndex=0;
    let rightIndex=arr.length-1;
    while(leftIndex<=rightIndex){
        let middleIndex=Math.floor((leftIndex+rightIndex)/2);
        if(target==arr[middleIndex])
        return middleIndex;
        else if(target<arr[middleIndex])
        rightIndex=middleIndex-1;
        else
        leftIndex=middleIndex+1;
    }
    return -1
}
console.log(binarySearch([2,5,6,7,1],6))
function search(arr,target,leftIndex,rightIndex){
    if(leftIndex>rightIndex)
    return -1
    let middleIndex=Math.floor((leftIndex+rightIndex)/2);
    if(target==arr[middleIndex])
    return middleIndex;
    else if(target<arr[middleIndex])
    return search(arr,target,leftIndex,middleIndex-1)
    else
    return search(arr,target,middleIndex+1,rightIndex);
    
}
//Big-o o(logn)
function binarySearchRecursion(arr,target){
    arr=arr.sort((a,b)=>a-b);
    return search(arr,target,0,arr.length-1)
}
console.log(binarySearchRecursion([3,6,5,2],31))
//Big-o o(n^2)
function bubbleSort(arr){
    let swapped
    do {
        swapped=false;
        for(let i=0;i<arr.length-1;i++){
            if(arr[i]>arr[i+1]){
                let temp=arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=temp;
                swapped=true;
            }
        }
    } while (swapped);
    return arr
}
console.log(bubbleSort([3,5,2,1,7,10]))
//Big-o o(n^2)
function innsertionSort(arr){
    for(let i=1;i<arr.length;i++){
        let numberToInsert=arr[i];
        let j=i-1;
        while(j >=0 && arr[j]>numberToInsert){
            arr[j+1]=arr[j];
            j=j-1;
        }
        arr[j+1]=numberToInsert;
    }
    return arr;
}
console.log(innsertionSort([-3,5,2,4]))

function quickSort(arr){
    if(arr.length<2)
    return arr;
    let pivot=arr[arr.length-1];
    let leftArray=[];
    let rightArray=[];
    for(let i=0;i<arr.length-1;i++){
        if(pivot>arr[i])
        leftArray.push(arr[i])
        else
        rightArray.push(arr[i])
    }
    return [...quickSort(leftArray),pivot,...quickSort(rightArray)]
}
console.log(quickSort([4,33,-1,4,2,8]))
function merge(leftArray,rightArray){
    let sortedArray=[];
    while(leftArray.length && rightArray.length){
        if(leftArray[0]<rightArray[0])
        sortedArray.push(leftArray.shift())
        else
        sortedArray.push(rightArray.shift())
    }
    return [...sortedArray,...leftArray,...rightArray]
}
function mergeSort(arr){
    if(arr.length<2)
    return arr;
    let mid=Math.floor(arr.length/2);
    let leftArray=arr.slice(0,mid);
    let rightArray=arr.slice(mid);
    return merge(mergeSort(leftArray),mergeSort(rightArray));
}
console.log(mergeSort([3,5,2,1,-5,2]))

function clibingStairsCase(n){
    const noOfWays=[1,2];
    for(let i=2;i<=n;i++){
        noOfWays[i]=noOfWays[i-1]+noOfWays[i-2];
    }
    return noOfWays[n-1]
}
console.log(clibingStairsCase(5))

function towerOfHanio(n,fromRod,toRod,usingRod){
    if(n==1){
        console.log(`Move disk 1 ${fromRod} to ${toRod}`)
        return
    }
    towerOfHanio(n-1,fromRod,usingRod,toRod)
    console.log(`Move disk ${n} ${fromRod} to ${toRod}`)
    towerOfHanio(n-1,usingRod,toRod,fromRod)
}
//towerOfHanio(3,'A','B','C')
console.log('a') && console.log('b')
console.log('a') || console.log('b')

//Find the Highest number in the array without 
//using javascript inbuild functions. 
var arr = [3, 14, [18, 4, 484, [101, 99], 4], [67, 2, 100], 0];
let highestNo=-Infinity;
function HighestNumber(arr){
    for(let i=0;i<arr.length;i++){
        if(Array.isArray(arr[i])){
            arr=[...HighestNumber(arr[i])]
        }else{
            highestNo=Math.max(highestNo,arr[i])
        }
    }
    return highestNo
}
console.log(HighestNumber(arr))