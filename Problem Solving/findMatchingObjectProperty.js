const obj = {
  a: 1,
  b: {
    c: [
      {
        d: 'd1',
      },
      {
        z: 'z1',
      },
    ],
    m: 'm2',
  },
};
 function findMatchingObjectProperty(obj,[key,value]){
  if(obj.hasOwnProperty(key) && obj[key]==value){
      return true
  }
  else{
      for(let item in obj){
         if(Array.isArray(obj[item])){
          for(let el of obj[item]){
              if(findMatchingObjectProperty(el,[key,value]))
                  return true
          }
         }
         else if(typeof obj[item]=='object' && obj[item] !=null){
              return findMatchingObjectProperty(obj[item],[key,value]);
         }
           
      }
  }
  return false;
  
 }
console.log(findMatchingObjectProperty(obj, ['z', 'z1'])); // Output: true
console.log(findMatchingObjectProperty(obj, ['d', 'd1'])); // Output: true
console.log(findMatchingObjectProperty(obj, ['z', 'd1'])); // Output: false