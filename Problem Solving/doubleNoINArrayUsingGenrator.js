function* doubleGenerator() {
    for (let value of this) {
      yield value * value;
    }
  }
  
  Array.prototype.double = doubleGenerator;
  
  let a = [2, 3, 5, 6];
  for (let v of a.double()) {
    console.log(v);
  }
  