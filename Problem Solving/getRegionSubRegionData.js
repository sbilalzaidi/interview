
let axios =require("axios");

const getRegionData=async()=>{
    try {
        let response= await axios.get('https://restcountries.com/v3.1/all');
        return response.data
    } catch (error) {
        console.error("Error in fetching",error);
        return [];
    }
}

async function groupBySubRegionAlongWithCountries(locations){
    const groupedLocation={};
    locations.forEach(location=>{
        let {subregion,name}=location;
        if(!groupedLocation[subregion])
            groupedLocation[subregion]=[name.common];
        else
        groupedLocation[subregion].push(name.common);
    })
    return groupedLocation
}


(async function fetchCoutriesData(){
    let location=await getRegionData();
    let output =await groupBySubRegionAlongWithCountries(location);
    console.log("output",output)
})();