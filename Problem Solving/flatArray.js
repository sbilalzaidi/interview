let arr=[1, [2,3, [4,5] ] ];
//output [1,2,3,4,5]
function flatern(arr){
    let output=[];
    for(let el of arr){
        if(Array.isArray(el))
            output.push(...flatern(el))
        else
        output.push(el)
    }
    return output;
}
console.log(flatern(arr))