//Length of Longest Subarray With at Most K Frequency
// Input: nums = [1,2,3,1,2,3,1,2], k = 2
// Output: 6
// Explanation: The longest possible good subarray is [1,2,3,1,2,3] 
//since the values 1, 2, and 3 occur at most twice in this subarray.
// Note that the subarrays [2,3,1,2,3,1] and [3,1,2,3,1,2] are also good.
// It can be shown that there are no good subarrays with length more than 6.

var maxSubarrayLength = function(nums, k) {
    let maxLength = 0;
    let subArray = {};
    let start = 0;
    
    for (let i = 0; i < nums.length; i++) {
        subArray[nums[i]] = (subArray[nums[i]] || 0) + 1;
        
        while (subArray[nums[i]] > k) {
            subArray[nums[start]] -= 1;
            if (subArray[nums[start]] === 0) {
                delete subArray[nums[start]];
            }
            start += 1;
        }
        
        maxLength = Math.max(maxLength, i - start + 1);
    }
    console.log("Sub Array",subArray)
    return maxLength;
};
