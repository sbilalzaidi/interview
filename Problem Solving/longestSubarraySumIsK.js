//program-2 : longest Subarray Whose Sum is equal to target sum
const arr = [1, 2, 3, 4, 5, 2];
const targetSum = 9;
//o/p = [2, 3, 4]

function longestSubarrayWithTargetSum(arr,k){
    let sum =0;
    let maxLength=0;
    let hashSum={};
    let start=0,end=0;
    if(arr.length==0)
        return [];//-1;
    for(let i=0;i<arr.length;i++){
        sum +=arr[i];
        if(sum==k){
            maxLength=i+1;
        }
        if(hashSum.hasOwnProperty(sum-k)){
         let startIndex=hashSum.hasOwnProperty(sum-k);
         maxLength=Math.max(maxLength,i-startIndex);
            if((i-startIndex)>=maxLength){
                start=startIndex;
                end=i
            }
        }
        if(!hashSum.hasOwnProperty(sum)){
            hashSum[sum]=i;
        }
    }
    console.log(start,end)
    return arr.slice(start,end);//maxLength
}

console.log(longestSubarrayWithTargetSum(arr,targetSum))