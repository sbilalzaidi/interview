class Queue{
    constructor(){
        this.items=[]
    }
    enqueue(element){
        this.items.push(element)
    }
    dequeue(element){
        this.items.unshift(element)
    }
    isEmpty(){
        return this.items.length==0;
    }
    peek(){
        if(this.items.length)
        return this.items[0]
    }
    size(){
        return this.items.length;
    }
    print(){
        console.log(this.items.toString())
    }
}

const queue=new Queue()
console.log(queue.enqueue(5))
console.log(queue.enqueue(52))
console.log(queue.isEmpty())
console.log(queue.print())