Table: Logs

+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| id          | int     |
| num         | varchar |
+-------------+---------+
In SQL, id is the primary key for this table.
id is an autoincrement column.
 

Find all numbers that appear at least three times consecutively.

Return the result table in any order.

with cte as (
    select id,num,lead(num) over(order by id) as next ,lag(num) over(order by id) as pre from Logs 
)
select num as ConsecutiveNums from cte where num=next and num=pre;