Table: Employee

+-------------+------+
| Column Name | Type |
+-------------+------+
| id          | int  |
| salary      | int  |
+-------------+------+
id is the primary key (column with unique values) for this table.
Each row of this table contains information about the salary of an employee.
 

Write a solution to find the second highest salary from the Employee table. If there is no second highest salary, return null (return None in Pandas).

select max(salary) as SecondHighestSalary from Employee where salary<(select max(salary) from Employee);
 
FOR Nth salary
 CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
BEGIN
SET N=N-1;
  RETURN (select IFNULL((select distinct salary from employee order by salary desc limit 1 OFFSET N),Null)
  );
END