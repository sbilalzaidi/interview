SELECT MAX(value) AS max_value
FROM numbers;

SELECT AVG(score) AS average_score
FROM scores;

SELECT SUBSTRING(full_name, 1, 3) AS first_three_characters
FROM names;

SELECT RIGHT(word, 1) AS last_character
FROM words;

SELECT LENGTH(sentence) AS sentence_length
FROM sentences;

SELECT LEFT(word, 1) AS first_character
FROM words;

SELECT CURRENT_DATE AS current_date;
SELECT NOW();

SELECT 
    YEAR(some_date_column) AS year,
    MONTH(some_date_column) AS month,
    DAY(some_date_column) AS day
FROM your_table;

SELECT *
FROM your_table
WHERE some_date_column > '2023-01-01';

-- Add 5 days to a date
SELECT DATEADD(day, 5, your_date_column) AS new_date
FROM your_table;

-- Subtract 1 month from a date
SELECT DATE_SUB(your_date_column, INTERVAL 1 MONTH) AS new_date
FROM your_table;

SELECT DATE_FORMAT(your_date_column, '%Y-%m-%d') AS formatted_date
FROM your_table;
