CREATE TABLE orders (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_name VARCHAR(100),
    quantity INT
);

CREATE TABLE order_logs (
    log_id INT AUTO_INCREMENT PRIMARY KEY,
    action VARCHAR(50),
    product_name VARCHAR(100),
    quantity INT,
    log_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

// Create afte insert trigger

DELIMITER //

CREATE TRIGGER order_insert_trigger AFTER INSERT ON orders
FOR EACH ROW
BEGIN
    INSERT INTO order_logs (action, product_name, quantity)
    VALUES ('Inserted', NEW.product_name, NEW.quantity);
END;
//

DELIMITER ;


Note:Mysql have 6 trigger befor/after intert update delete