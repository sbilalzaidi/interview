SELECT DISTINCT column, AGG_FUNC(column_or_expression), …
FROM mytable
    JOIN another_table
      ON mytable.column = another_table.column
    WHERE constraint_expression
    GROUP BY column
    HAVING constraint_expression
    ORDER BY column ASC/DESC
    LIMIT count OFFSET COUNT;

Sql query execution order:
1.FROM and JOINS clause:
2.WHERE clause:
3.GROUP BY clause:
4.HAVING clause:
5.SELECT clause:
6.DISTINCT clause:
7.ORDER BY clause:
8.LIMIT / OFFSET clause: