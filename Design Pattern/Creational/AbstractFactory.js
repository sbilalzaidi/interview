/**
 * Abstract Factory is a creational design pattern 
 * that lets you produce families of related objects 
 * without specifying their concrete classes.
 */

// Abstract product A
class AbstractProductA {
    constructor() {
      if (new.target === AbstractProductA) {
        throw new Error('Abstract class cannot be instantiated directly');
      }
    }
    methodA() {
      throw new Error('Method methodA() must be implemented');
    }
  }
  
  // Concrete product A1
  class ConcreteProductA1 extends AbstractProductA {
    methodA() {
      return 'Product A1 method';
    }
  }
  
  // Concrete product A2
  class ConcreteProductA2 extends AbstractProductA {
    methodA() {
      return 'Product A2 method';
    }
  }
  
  // Abstract product B
  class AbstractProductB {
    constructor() {
      if (new.target === AbstractProductB) {
        throw new Error('Abstract class cannot be instantiated directly');
      }
    }
    methodB() {
      throw new Error('Method methodB() must be implemented');
    }
  }
  
  // Concrete product B1
  class ConcreteProductB1 extends AbstractProductB {
    methodB() {
      return 'Product B1 method';
    }
  }
  
  // Concrete product B2
  class ConcreteProductB2 extends AbstractProductB {
    methodB() {
      return 'Product B2 method';
    }
  }
  
  // Abstract factory
  class AbstractFactory {
    createProductA() {
      throw new Error('Method createProductA() must be implemented');
    }
  
    createProductB() {
      throw new Error('Method createProductB() must be implemented');
    }
  }
  
  // Concrete factory 1
  class ConcreteFactory1 extends AbstractFactory {
    createProductA() {
      return new ConcreteProductA1();
    }
  
    createProductB() {
      return new ConcreteProductB1();
    }
  }
  
  // Concrete factory 2
  class ConcreteFactory2 extends AbstractFactory {
    createProductA() {
      return new ConcreteProductA2();
    }
  
    createProductB() {
      return new ConcreteProductB2();
    }
  }
  
  // Usage
  const factory1 = new ConcreteFactory1();
  const productA1 = factory1.createProductA();
  const productB1 = factory1.createProductB();
  
  console.log(productA1.methodA()); // Output: Product A1 method
  console.log(productB1.methodB()); // Output: Product B1 method
  
  const factory2 = new ConcreteFactory2();
  const productA2 = factory2.createProductA();
  const productB2 = factory2.createProductB();
  
  console.log(productA2.methodA()); // Output: Product A2 method
  console.log(productB2.methodB()); // Output: Product B2 method
  