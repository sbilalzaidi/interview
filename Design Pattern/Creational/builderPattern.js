class House{
    constructor(){
        this.windows=0;
        this.doors=0;
        this.rooms=0;
        this.hasGarden=false;
    }
    setWindows(windows){
        this.windows=windows
    }
    setDoors(doors){
        this.doors=doors;
    }
    setRooms(rooms){
        this.rooms=rooms;
    }
    setGarden(hasGarden){
        this.hasGarden=hasGarden;
    }
    getDescription(){
        return `House with ${this.windows} windows,${this.doors} doors,
        ${this.rooms} rooms,${this.hasGarden} garden`
    }
}
//The Builder Class
class HouseBuilder{
    constructor(){
        this.house=new House();
    }
    setWindows(windows){
        this.house.setWindows(windows);
        return this;
    }
    setDoors(doors){
        this.house.setDoors(doors);
        return this;
    }
    setRooms(rooms){
        this.house.setRooms(rooms);
        return this;
    }
    setGarden(hasGarden){
        this.house.setGarden(hasGarden);
        return this
    }
    build(){
        return this.house;
    }
}
//usage
const houseBuilder=new HouseBuilder();
const myhouse=houseBuilder.setWindows(12).setDoors(2).setRooms(2).setGarden(true).build();

console.log(myhouse.getDescription())
