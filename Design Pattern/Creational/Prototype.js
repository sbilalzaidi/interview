/**
 * Prototype is a creational design pattern that lets 
 * you copy existing objects without
 *  making your code dependent on their classes.
 */

// Prototype object
const prototypeObject = {
    name: "Prototype",
    greet: function() {
      return `Hello, I'm ${this.name}.`;
    }
  };
  
  // Create new objects based on the prototype
  const object1 = Object.create(prototypeObject);
  const object2 = Object.create(prototypeObject);
  
  // Modify properties of the new objects
  object1.name = "Object 1";
  object2.name = "Object 2";
  
  // Usage
  console.log(object1.greet()); // Output: Hello, I'm Object 1.
  console.log(object2.greet()); // Output: Hello, I'm Object 2.
  