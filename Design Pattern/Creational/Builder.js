/**
 * Builder is a creational design 
 * pattern that lets you construct complex objects 
 * step by step. The pattern allows you to produce
 *  different types and representations of an object
 *  using the same construction code.
 */
class Product {
    constructor() {
      // Set default values
      this.name = '';
      this.price = 0;
      this.category = '';
      this.description = '';
    }
  }
  
  class ProductBuilder {
    constructor() {
      // Initialize with a new instance of the Product class
      this.product = new Product();
    }
  
    setName(name) {
      this.product.name = name;
      return this; // Return this for method chaining
    }
  
    setPrice(price) {
      this.product.price = price;
      return this; // Return this for method chaining
    }
  
    setCategory(category) {
      this.product.category = category;
      return this; // Return this for method chaining
    }
  
    setDescription(description) {
      this.product.description = description;
      return this; // Return this for method chaining
    }
  
    build() {
      // Return the fully constructed Product object
      return this.product;
    }
  }
  
  // Usage
  const product = new ProductBuilder()
    .setName('Product A')
    .setPrice(20)
    .setCategory('Category A')
    .setDescription('Description of Product A')
    .build();
  
  console.log(product);
  