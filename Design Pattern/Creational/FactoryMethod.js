/**
 * Factory Method is a creational design pattern 
 * that provides an interface for creating objects in a superclass, 
 * but allows subclasses to alter the type of objects that will be created.
 */

// Product interface
class Product {
    constructor(name) {
      this.name = name;
    }
  }
  
  // Concrete products
  class ConcreteProduct1 extends Product {
    constructor(name) {
      super(name);
    }
  }
  
  class ConcreteProduct2 extends Product {
    constructor(name) {
      super(name);
    }
  }
  
  // Factory Method
  function createProduct(type, name) {
    switch (type) {
      case 'product1':
        return new ConcreteProduct1(name);
      case 'product2':
        return new ConcreteProduct2(name);
      default:
        throw new Error('Invalid product type.');
    }
  }
  
  // Usage
  const product1 = createProduct('product1', 'Product 1');
  const product2 = createProduct('product2', 'Product 2');
  
  console.log(product1); // Output: ConcreteProduct1 { name: 'Product 1' }
  console.log(product2); // Output: ConcreteProduct2 { name: 'Product 2' }
  