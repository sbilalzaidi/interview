/**
 * Singleton is a creational design pattern that
 *  lets you ensure that a class has only one instance, 
 * while providing a global access point to this instance.
 */
class Singleton {
    constructor() {
      if (!Singleton.instance) {
        Singleton.instance = this;
      }
  
      return Singleton.instance;
    }
  
    // Example method
    someMethod() {
      console.log("This is a method of the singleton instance.");
    }
  }
  
  // Usage
  const instance1 = new Singleton();
  const instance2 = new Singleton();
  
  console.log(instance1 === instance2); // Output: true
  
  instance1.someMethod(); // Output: "This is a method of the singleton instance."
  instance2.someMethod(); // Output: "This is a method of the singleton instance."
  