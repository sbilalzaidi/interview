class House{
    constructor(){
        this.windows=0;
        this.doors=0;
        this.rooms=0;
        this.hasGarden=false;
    }
    setWindows(windows){
        this.windows=windows;
        return this;
    }
    setDoors(doors){
        this.doors=doors;
        return this;
    }
    setRooms(rooms){
        this.rooms=rooms;
        return this;
    }
    setGarden(hasGarden){
        this.hasGarden=hasGarden;
        return this;
    }
    getDescription(){
        return `House with ${this.windows} windows,${this.doors} doors,
        ${this.rooms} rooms,${this.hasGarden} garden`
    }
    build(){
        return this;
    }
}

const myHouse =new House().setDoors(2).setRooms(2).setGarden(true).build();

console.log(myHouse)