/**
 * Decorator is a structural design pattern 
 * that lets you attach new behaviors to objects by placing these 
 * objects inside special wrapper objects that contain the behaviors.
 */
// Component interface
class Component {
    operation() {
      throw new Error('Operation must be implemented');
    }
  }
  
  // Concrete component
  class ConcreteComponent extends Component {
    operation() {
      return 'ConcreteComponent operation';
    }
  }
  
  // Base decorator
  class Decorator extends Component {
    constructor(component) {
      super();
      this.component = component;
    }
  
    operation() {
      return this.component.operation();
    }
  }
  
  // Concrete decorator A
  class ConcreteDecoratorA extends Decorator {
    operation() {
      return `Decorator A (${this.component.operation()})`;
    }
  }
  
  // Concrete decorator B
  class ConcreteDecoratorB extends Decorator {
    operation() {
      return `Decorator B (${this.component.operation()})`;
    }
  }
  
  // Usage
  const component = new ConcreteComponent();
  const decoratedComponentA = new ConcreteDecoratorA(component);
  const decoratedComponentB = new ConcreteDecoratorB(decoratedComponentA);
  
  console.log(decoratedComponentB.operation());
  