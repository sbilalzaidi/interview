/**
 * Proxy is a structural design pattern that
 *  lets you provide a substitute or placeholder
 *  for another object. A proxy controls access to the 
 * original object, allowing you to perform something either 
 * before or after the request gets through to the original object.
 */
// Real subject
class RealSubject {
    request() {
      return 'RealSubject: Handling request';
    }
  }
  
  // Proxy
  class ProxySubject {
    constructor(realSubject) {
      this.realSubject = realSubject;
    }
  
    request() {
      if (this.checkAccess()) {
        return this.realSubject.request();
      } else {
        return 'ProxySubject: Access denied';
      }
    }
  
    checkAccess() {
      // Check access logic goes here
      return true; // For simplicity, always allow access
    }
  }
  
  // Usage
  const realSubject = new RealSubject();
  const proxy = new ProxySubject(realSubject);
  
  console.log(proxy.request()); // Output: RealSubject: Handling request
  