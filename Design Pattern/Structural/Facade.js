/**
 * Facade is a structural design pattern 
 * that provides a simplified interface 
 * to a library, a framework, or any other complex set of classes.
 */
// Complex subsystem
class SubsystemA {
    operationA() {
      return 'Subsystem A operation';
    }
  }
  
  class SubsystemB {
    operationB() {
      return 'Subsystem B operation';
    }
  }
  
  class SubsystemC {
    operationC() {
      return 'Subsystem C operation';
    }
  }
  
  // Facade
  class Facade {
    constructor() {
      this.subsystemA = new SubsystemA();
      this.subsystemB = new SubsystemB();
      this.subsystemC = new SubsystemC();
    }
  
    operation() {
      let result = 'Facade initializes subsystems:\n';
      result += this.subsystemA.operationA() + '\n';
      result += this.subsystemB.operationB() + '\n';
      result += this.subsystemC.operationC();
      return result;
    }
  }
  
  // Usage
  const facade = new Facade();
  console.log(facade.operation());
  