/**
 * Adapter is a structural design pattern that allows
 * objects with incompatible interfaces to collaborate.
 */
// Target interface
class Target {
    request() {
      throw new Error('Request method must be implemented.');
    }
  }
  
  // Adaptee (the incompatible interface)
  class Adaptee {
    specificRequest() {
      return 'Adaptee specific request.';
    }
  }
  
  // Adapter (class adapter)
  class Adapter extends Target {
    constructor(adaptee) {
      super();
      this.adaptee = adaptee;
    }
  
    request() {
      return this.adaptee.specificRequest();
    }
  }
  
  // Usage
  const adaptee = new Adaptee();
  const adapter = new Adapter(adaptee);
  console.log(adapter.request()); // Output: Adaptee specific request.
  