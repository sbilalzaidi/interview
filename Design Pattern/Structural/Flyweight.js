/**
 * Flyweight is a structural design pattern that 
 * lets you fit more objects into the available 
 * amount of RAM by sharing common parts of state
 *  between multiple objects instead of keeping all 
 * of the data in each object.
 */
// Flyweight factory
class FlyweightFactory {
    constructor() {
      this.flyweights = {};
    }
  
    getFlyweight(key) {
      if (!this.flyweights[key]) {
        this.flyweights[key] = new ConcreteFlyweight(key);
      }
      return this.flyweights[key];
    }
  
    getFlyweightsCount() {
      return Object.keys(this.flyweights).length;
    }
  }
  
  // Flyweight interface
  class Flyweight {
    operation(extrinsicState) {
      throw new Error('Operation must be implemented');
    }
  }
  
  // Concrete flyweight
  class ConcreteFlyweight extends Flyweight {
    constructor(intrinsicState) {
      super();
      this.intrinsicState = intrinsicState;
    }
  
    operation(extrinsicState) {
      return `ConcreteFlyweight with intrinsic state: ${this.intrinsicState}, and extrinsic state: ${extrinsicState}`;
    }
  }
  
  // Client
  class Client {
    constructor(flyweightFactory) {
      this.flyweightFactory = flyweightFactory;
    }
  
    operation(key, extrinsicState) {
      const flyweight = this.flyweightFactory.getFlyweight(key);
      return flyweight.operation(extrinsicState);
    }
  }
  
  // Usage
  const flyweightFactory = new FlyweightFactory();
  const client = new Client(flyweightFactory);
  
  console.log(client.operation('shared', 'A')); // Output: ConcreteFlyweight with intrinsic state: shared, and extrinsic state: A
  console.log(client.operation('shared', 'B')); // Output: ConcreteFlyweight with intrinsic state: shared, and extrinsic state: B
  console.log(client.operation('unique', 'C')); // Output: ConcreteFlyweight with intrinsic state: unique, and extrinsic state: C
  
  console.log(`Number of flyweights created: ${flyweightFactory.getFlyweightsCount()}`); // Output: Number of flyweights created: 2
  