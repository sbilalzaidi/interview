/**
 * Bridge is a structural design pattern that lets you 
 * split a large class or a set of closely related classes
 *  into two separate hierarchies—abstraction and
 *  implementation—which can be developed independently of each other.
 */
// Abstraction
class Abstraction {
    constructor(implementation) {
      this.implementation = implementation;
    }
  
    operation() {
      const result = this.implementation.operationImplementation();
      return `Abstraction: ${result}`;
    }
  }
  
  // Implementation
  class Implementation {
    operationImplementation() {
      throw new Error('Operation implementation must be defined');
    }
  }
  
  // Concrete implementation A
  class ConcreteImplementationA extends Implementation {
    operationImplementation() {
      return 'Concrete Implementation A';
    }
  }
  
  // Concrete implementation B
  class ConcreteImplementationB extends Implementation {
    operationImplementation() {
      return 'Concrete Implementation B';
    }
  }
  
  // Usage
  const implementationA = new ConcreteImplementationA();
  const abstractionA = new Abstraction(implementationA);
  console.log(abstractionA.operation()); // Output: Abstraction: Concrete Implementation A
  
  const implementationB = new ConcreteImplementationB();
  const abstractionB = new Abstraction(implementationB);
  console.log(abstractionB.operation()); // Output: Abstraction: Concrete Implementation B
  