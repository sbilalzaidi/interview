/**
 * Composite is a structural design
 *  pattern that lets you compose objects into tree structures and
 *  then work with these structures as if they were individual objects.
 */
// Component
class Component {
    constructor(name) {
      this.name = name;
    }
  
    // Abstract method
    operation() {
      throw new Error('Operation must be implemented');
    }
  }
  
  // Leaf
  class Leaf extends Component {
    operation() {
      return `Leaf ${this.name} operation`;
    }
  }
  
  // Composite
  class Composite extends Component {
    constructor(name) {
      super(name);
      this.children = [];
    }
  
    add(component) {
      this.children.push(component);
    }
  
    remove(component) {
      const index = this.children.indexOf(component);
      if (index !== -1) {
        this.children.splice(index, 1);
      }
    }
  
    operation() {
      const results = [];
      for (const child of this.children) {
        results.push(child.operation());
      }
      return `Composite ${this.name} operation:\n${results.join('\n')}`;
    }
  }
  
  // Usage
  const leaf1 = new Leaf('1');
  const leaf2 = new Leaf('2');
  const leaf3 = new Leaf('3');
  
  const composite1 = new Composite('A');
  composite1.add(leaf1);
  composite1.add(leaf2);
  
  const composite2 = new Composite('B');
  composite2.add(leaf3);
  
  const composite = new Composite('Root');
  composite.add(composite1);
  composite.add(composite2);
  
  console.log(composite.operation());
  