class AsyncEventEmitter {
    constructor() {
      this.events = {};
    }
  
    on(eventName, callback) {
      if (!this.events[eventName]) {
        this.events[eventName] = [];
      }
      this.events[eventName].push(callback);
    }
  
    async emit(eventName, ...args) {
      if (this.events[eventName]) {
        for (const callback of this.events[eventName]) {
          await callback(...args);
        }
      }
    }
  
    off(eventName, callback) {
      if (this.events[eventName]) {
        this.events[eventName] = this.events[eventName].filter(cb => cb !== callback);
      }
    }
  }
  
  // Example usage:
  const asyncEmitter = new AsyncEventEmitter();
  
  // Subscribe to an event
  asyncEmitter.on('message', async (message) => {
    console.log('Received message:', message);
    await delay(1000); // Simulate asynchronous operation
    console.log('Message processed:', message);
  });
  
  // Emit an event
  asyncEmitter.emit('message', 'Hello, world!');
  
  // Helper function to simulate asynchronous delay
  function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  