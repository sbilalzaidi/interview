const express = require('express');
const cookieParser = require('cookie-parser');
const csurf = require('csurf');

const app = express();

// Middleware to parse cookies
app.use(cookieParser());

// Middleware to generate and verify CSRF tokens
app.use(csurf({ cookie: true }));

// Middleware to set CSRF token in response headers
app.use((req, res, next) => {
    res.setHeader('X-CSRF-Token', req.csrfToken());
    next();
});

// Route to handle form submissions
app.post('/submit', (req, res) => {
    // Verify CSRF token
    if (!req.csrfToken()) {
        return res.status(403).send('CSRF token validation failed');
    }

    // Process form submission
    // ...
    res.send('Form submitted successfully');
});

// Start the server
app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
