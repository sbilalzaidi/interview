const express = require('express');
const csrf = require('csurf');
const cookieParser = require('cookie-parser');
const csrfProtection = csrf({ cookie: true });
const app = express();
app.set('view engine', 'ejs')
app.use(express.json());
app.use(cookieParser());
 
// for all request
app.use(csrfProtection);
// This middleware adds a CSRF token to all requests
app.use((req, res, next) => {
    res.locals.csrfToken = req.csrfToken();
    next();
  });
  
  // Route example
app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.get('/form', csrfProtection, function (req, res) {
    // pass the csrfToken to the view
    res.render('login', { csrfToken: req.csrfToken() });
});
 
app.post('/process',csrfProtection, function (req, res) {
    res.send('Successfully Validated!!');
});
 
app.listen(3000, (err) => {
    if (err) console.log(err);
    console.log('Server Running');
});