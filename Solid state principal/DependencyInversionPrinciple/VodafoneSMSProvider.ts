import { vodafoneConfig } from './config';
import { ISMSSender } from './ISMSSender.interface';
import { IVodafoneConfig } from './IVodafoneConfig.interface';
import { VodafoneApi } from './vodafone.lib';

export class VodafoneSMSGateway implements ISMSSender {
  vodafoneInstance = new VodafoneApi();
  constructor() {
    this.connect();
  }
  connect() {
    //connect to vodafone gateway using vodafoneConfig object.
    this.vodafoneInstance.config({});
  }

  sendMessage(recipient: string, message: string): void {
    this.vodafoneInstance.sendMessage(message,[recipient]);
    console.log(`Sending SMS to ${recipient}: ${message}`);
  }
}