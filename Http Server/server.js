const express = require('express');

const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Dummy data
const users = [
  { id: 1, username: 'john_doe', password: 'password123', email: 'john@example.com' ,token:''},
  { id: 2, username: 'jane_doe', password: 'password456', email: 'jane@example.com' ,token:''}
];
// Dummy authentication middleware
const authenticateUser = (req, res, next) => {
  const { token } = req.headers;
  const user = users.find(u => u.token === token);
  if (!user || !token) {
    return res.status(401).json({ error: 'Unauthorized' });
  }
  req.user = user;
  next();
};
//validate user
const checkUserNameAndPassword = (req, res, next) => {
    const { username, password } = req.body;
    const user = users.find(u => u.username === username && u.password === password);
    if (!user) {
      return res.status(401).json({ error: 'Unauthorized' });
    }
    // Find the user in the array
    const userIndex = users.findIndex(u => u.username === username && u.password === password);

  // If the user is found, update the specified key
  if (userIndex !== -1) {
    users[userIndex]['token'] = (new Date()).getTime();
    req.user = user;
    next();
  }
}
// Routes
app.put('/api/users/login', checkUserNameAndPassword,async (req, res) => {
    const user=req;
    res.status(200).json(user);
});
// 200 OK
app.get('/api/users', authenticateUser, (req, res) => {
  res.status(200).json(users);
});

// 201 Created
app.post('/api/users', authenticateUser, (req, res) => {
  const newUser = req.body;
  const userIndex = users.findIndex(u => u.id === newUser.userId);
  if(userIndex !== -1)
  res.status(409).json({ message: `User already exists.` });
  else{
    users.push(newUser);
    res.status(201).json(newUser);
  }
});

// 202 Accepted
app.put('/api/users/:id', authenticateUser, (req, res) => {
  const userId = parseInt(req.params.id);
  // Perform some async operation, then respond with 202
  const userIndex = users.findIndex(u => u.id === userId);
  if(userIndex !== -1)
  res.status(202).json({ message: `Update accepted for user ${userId}` });
  else
  res.status(404).json({ message: `User not found` });
});

// 400 Bad Request
app.get('/api/bad-request', (req, res) => {
  res.status(400).json({ error: 'Bad Request' });
});

// 403 Forbidden
app.get('/api/forbidden', (req, res) => {
  res.status(403).json({ error: 'Forbidden' });
});

// 500 Internal Server Error
app.get('/api/internal-error', (req, res) => {
  // Simulate an internal server error
  throw new Error('Simulated Internal Server Error');
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
