const express = require('express');
const passport = require('passport');
const session = require('express-session');
const { Strategy: OpenIDConnectStrategy } = require('passport-openidconnect');

const app = express();

// Initialize express session middleware
app.use(session({
    secret: 'your-secret-key',
    resave: false,
    saveUninitialized: true
}));

// Initialize Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Configure OpenID Connect Strategy
passport.use(new OpenIDConnectStrategy({
    issuer: 'https://your-openid-provider.com', // Replace with your OpenID Connect provider's issuer URL
    clientID: 'your-client-id',
    clientSecret: 'your-client-secret',
    redirectURI: 'http://localhost:3000/auth/callback', // Your callback URL
    scope: 'openid profile email' // Requested scopes
}, (issuer, sub, profile, accessToken, refreshToken, params, done) => {
    return done(null, profile);
}));

// Serialize and deserialize user
passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});

// Route to initiate OpenID Connect authentication
app.get('/auth', passport.authenticate('openidconnect'));

// Route to handle OpenID Connect callback
app.get('/auth/callback', passport.authenticate('openidconnect', {
    successRedirect: '/profile',
    failureRedirect: '/login'
}));

// Route to display user profile after successful authentication
app.get('/profile', (req, res) => {
    res.send(req.user); // req.user contains the authenticated user's profile
});

// Route to handle login failure
app.get('/login', (req, res) => {
    res.send('Login failed');
});

// Start the server
app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
