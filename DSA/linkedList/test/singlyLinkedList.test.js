const LinkedList = require('./singlyLinkedList');

describe('SingleLinkedList', () => {
    let singlyLinkedList;

    beforeEach(() => {
        singlyLinkedList = new LinkedList(10);
    });

    test('should create a linked list with initial value', () => {
        expect(singlyLinkedList.head.value).toBe(10);
        expect(singlyLinkedList.tail.value).toBe(10);
        expect(singlyLinkedList.length).toBe(1);
    });

    test('should append values to the linked list', () => {
        singlyLinkedList.append(5);
        singlyLinkedList.append(16);
        expect(singlyLinkedList.tail.value).toBe(16);
        expect(singlyLinkedList.length).toBe(3);
    });

    test('should prepend values to the linked list', () => {
        singlyLinkedList.prepend(1);
        expect(singlyLinkedList.head.value).toBe(1);
        expect(singlyLinkedList.length).toBe(2);
    });

    test('should insert values at a given index', () => {
        singlyLinkedList.append(5);
        singlyLinkedList.append(16);
        singlyLinkedList.insert(2, 99);
        expect(singlyLinkedList.head.next.next.value).toBe(99);
        expect(singlyLinkedList.length).toBe(4);
    });

    test('should remove values at a given index', () => {
        singlyLinkedList.append(5);
        singlyLinkedList.append(16);
        singlyLinkedList.remove(1);
        expect(singlyLinkedList.head.next.value).toBe(16);
        expect(singlyLinkedList.length).toBe(2);
    });

    test('should search for values in the linked list', () => {
        singlyLinkedList.append(5);
        singlyLinkedList.append(16);
        expect(singlyLinkedList.search(5)).toBe(1);
        expect(singlyLinkedList.search(16)).toBe(2);
        expect(singlyLinkedList.search(100)).toBe(-1);
    });
});
