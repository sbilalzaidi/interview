class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class LinkedList {
  constructor(value) {
    this.head = {
      value: value,
      next: null
    };
    this.tail = this.head;
    this.length = 1;
  }

  printList() {
    let currentNode = this.head;
    let list = '';
    while (currentNode !== null) {
      list += currentNode.value;
      if (currentNode.next !== null) {
        list += ' --> ';
      }
      currentNode = currentNode.next;
    }
    console.log(list);
  }

  append(value) {
    let newNode = new Node(value);
    this.tail.next = newNode;
    this.tail = newNode;
    this.length++;
    this.printList();
    return this.length;
  }

  prepend(value) {
    let newNode = new Node(value);
    newNode.next = this.head;
    this.head = newNode;
    this.length++;
    this.printList();
    return this.length;
  }

  insert(index, value) {
    if (!Number.isInteger(index) || index < 0 || index > this.length) {
      console.log(`Enter valid index. Current length of the linked list is ${this.length}`);
      return this.length;
    }

    if (index === 0) {
      this.prepend(value);
      return this.length;
    }

    if (index === this.length) {
      this.append(value);
      return this.length;
    }

    let newNode = new Node(value);
    let previousNode = this.head;
    for (let k = 0; k < index - 1; k++) {
      previousNode = previousNode.next;
    }
    let nextNode = previousNode.next;
    newNode.next = nextNode;
    previousNode.next = newNode;

    this.length++;
    this.printList();
    return this.length;
  }

  remove(index) {
    if (!Number.isInteger(index) || index < 0 || index >= this.length) {
      console.log(`Enter valid index. Current length of the linked list is ${this.length}`);
      return this.length;
    }

    if (index === 0) {
      this.head = this.head.next;
      this.length--;
      if (this.length === 0) {
        this.tail = null;
      }
      this.printList();
      return this.length;
    }

    let previousNode = this.head;
    for (let k = 0; k < index - 1; k++) {
      previousNode = previousNode.next;
    }
    let nodeToRemove = previousNode.next;
    previousNode.next = nodeToRemove.next;

    if (index === this.length - 1) {
      this.tail = previousNode;
    }

    this.length--;
    this.printList();
    return this.length;
  }

  search(value) {
    let currentNode = this.head;
    let index = 0;
    while (currentNode !== null) {
      if (currentNode.value === value) {
        console.log(`Value ${value} found at index ${index}`);
        return index;
      }
      currentNode = currentNode.next;
      index++;
    }
    console.log(`Value ${value} not found`);
    return -1;
  }
}

module.exports = LinkedList;
