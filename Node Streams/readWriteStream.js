const fs = require('fs');

// File paths
const sourceFilePath = 'source.txt';
const destinationFilePath = 'destination.txt';

// Create a readable stream from the source file
const readStream = fs.createReadStream(sourceFilePath, { encoding: 'utf8' });

// Create a writable stream to the destination file
const writeStream = fs.createWriteStream(destinationFilePath, { encoding: 'utf8' });

// Set up event listeners for the read stream
readStream.on('data', (chunk) => {
  // Data is read in chunks
  console.log(`Read chunk: ${chunk}`);
  
  // Write the chunk to the destination file
  writeStream.write(chunk);
});

// Event listener for the end of the read stream
readStream.on('end', () => {
  console.log('Read stream ended');

  // Close the write stream after writing is complete
  writeStream.end();
});

// Event listener for the finish event on the write stream
writeStream.on('finish', () => {
  console.log('Write stream finished');
  console.log('File read and write operation complete.');
});

// Event listener for errors on the read stream
readStream.on('error', (err) => {
  console.error('Read stream error:', err);
});

// Event listener for errors on the write stream
writeStream.on('error', (err) => {
  console.error('Write stream error:', err);
});

// Handle errors on the write stream (e.g., if the destination file cannot be opened for writing)
writeStream.on('error', (err) => {
  console.error('Write stream error:', err);
});


