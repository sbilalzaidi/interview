const fs = require('fs');

// File paths
const sourceFilePath = 'source.txt';
const destinationFilePath = 'destination.txt';

// Create a readable stream from the source file
const readStream = fs.createReadStream(sourceFilePath, { encoding: 'utf8' });

// Create a writable stream to the destination file
const writeStream = fs.createWriteStream(destinationFilePath, { encoding: 'utf8' });

// Pipe the data from the read stream to the write stream
readStream.pipe(writeStream);

// Event listener for the finish event on the write stream
writeStream.on('finish', () => {
  console.log('File read and write operation complete.');
});

// Event listener for errors on the read stream
readStream.on('error', (err) => {
  console.error('Read stream error:', err);
});

// Event listener for errors on the write stream
writeStream.on('error', (err) => {
  console.error('Write stream error:', err);
});
