const fs = require('fs');
const { Transform } = require('stream');

// Create a transform stream for modifying data
const myTransformStream = new Transform({
  transform(chunk, encoding, callback) {
    // Modify the data (e.g., converting to uppercase)
    const modifiedChunk = chunk.toString().toUpperCase();
    this.push(modifiedChunk);
    callback();
  }
});

// Create a readable stream (e.g., reading from a file)
const readableStream = fs.createReadStream('source.txt');

// Create a writable stream (e.g., writing to another file)
const writableStream = fs.createWriteStream('destination.txt');

// Pipe the readable stream through the transform stream to the writable stream
readableStream.pipe(myTransformStream).pipe(writableStream);

// Handle events (optional)
readableStream.on('end', () => {
  console.log('Read operation completed.');
});

writableStream.on('finish', () => {
  console.log('Write operation completed.');
});
