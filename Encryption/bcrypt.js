const bcrypt = require('bcryptjs');

const plaintextPassword = 'password456';

bcrypt.hash(plaintextPassword, 10, (err, hash) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log('Hashed Password:', hash);

  // Verify the hashed password
  bcrypt.compare(plaintextPassword, hash, (err, result) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log('Password Matched:', result);
  });
});
