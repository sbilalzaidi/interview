//Rivest-Shamir-Adleman
const NodeRSA = require('node-rsa');

const key = new NodeRSA({ b: 512 }); // Generate a new 512-bit RSA key pair
const plaintext = 'Hello, World!';

const encrypted = key.encrypt(plaintext, 'base64');
console.log('Encrypted:', encrypted);

const decrypted = key.decrypt(encrypted, 'utf8');
console.log('Decrypted:', decrypted);
