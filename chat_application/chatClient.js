const net = require('net');
require('dotenv').config(); 
const {CHANNEL_OPRATOR} = require('./constants');
const readline = require('readline');
const logger = require('./logger'); 
const PORT = process.env.PORT;


class ChatClient {
  constructor(port) {
    this.port = port;
    this.client = null;
    this.rl = null;
    this.userName = '';
    this.connectToServer();
    this.clientInputHandler();
  }

  connectToServer() {
    this.client = net.createConnection({ port: this.port }, () => {
      logger.info('CLIENT: I connected to the server.');
    });

    this.client.on('data', (data) => {
      this.serverData(data);
    });

    this.client.on('end', () => {
      this.handleServerEnd();
    });
  }

  serverData(data) {
    const parsedData = JSON.parse(data.toString());
    logger.info(`Data received from server: ${JSON.stringify(parsedData)}`);
    this.setUserName(parsedData);
  }

  setUserName(data) {
    if (data?.op === 'change-name' && data?.userName) {
      this.userName = data?.userName;
      logger.info(`User name set to: ${this.userName}`);
    }
  }

  handleServerEnd() {
    logger.info('CLIENT: I disconnected from the server.');
  }

  clientInputHandler() {
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    this.rl.on('line', (userInput) => {
      try {
        const payload = JSON.parse(userInput);
        this.sendMessageToServer({
          ...payload,
          userName: payload?.op !== CHANNEL_OPRATOR.CHANGE_NAME ? this.userName : payload?.userName
        });
      } catch (e) {
        logger.error(`Invalid user input: ${e.message}`);
        logger.info('Invalid user input format. Please provide input in JSON format: {"message":"your message", "op":"change-name/@"}, userName: "required when user name is changed."');
      }
    });
  }

  sendMessageToServer(payload) {
    this.client.write(JSON.stringify(payload));
    logger.info(`Message sent to server: ${JSON.stringify(payload)}`);
  }

  validateUserMessage(payload) {
    if (!payload?.message) {
      throw new Error('Message is required');
    }
    if (payload?.op === CHANNEL_OPRATOR.CHANGE_NAME && !payload?.userName) {
      throw new Error('UserName is required.');
    }
    else if (op === CHANNEL_OPRATOR.PRIVATE_MESSAGE && !to)
      throw new Error('To is required.');
  }
}

new ChatClient(PORT);
