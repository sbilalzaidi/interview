const net = require('net');
const {DEFAULT_USER_NAME,CHANNEL_OPRATOR,MESSEGE_TYPE} = require('./constants');
const logger = require('./logger');
class ChatServer {
  constructor(port) {
    this.port = port;
    this.clients = [];
    this.usedIds = new Map();
    this.server = net.createServer(this.handleNewConnection.bind(this));
  }

  start() {
    this.server.listen(this.port, () => {
      logger.info(`Server opened on port ${this.port}`);
    });

    this.server.on('error', (e) => {
      if (e.code === 'EADDRINUSE') {
        logger.warn('Address in use, retrying...');
        setTimeout(() => {
          this.server.close();
          this.server.listen(this.port);
        }, 1000);
      } else {
        logger.error(`Server failed: ${e.message}`);
      }
    });
  }

  generateUniqueId(socket) {
    return `soc_${new Date().getTime()}-${socket.remoteAddress}-${socket.remotePort}`;
  }

  handleNewConnection(socket) {
    logger.info(`New client connected: ${socket.remoteAddress}:${socket.remotePort}`);
    socket.id = this.generateUniqueId(socket);
    socket.userName = `user${socket.id}`;
    this.clients.push(socket);
    this.broadcastUserList();
    this.socketHandlers(socket);
    this.sendUserName(socket);
  }

  broadcastUserList() {
    const clientNames = this.clients.map(client => `${client.userName || DEFAULT_USER_NAME} (${client.id})`).join('\n');
    const message = `CLIENT LIST: ${clientNames}`;
    this.clients.forEach(client => client.write(JSON.stringify({
      message: message
    })));
  }

  sendUserName(socket){
    setTimeout(() => {
      socket.write(JSON.stringify({
        message: `User is updated: ${socket.userName}`,
        userName: socket.userName,
        op: 'change-name'
      }));
    }, 1000);
  }

  socketHandlers(socket) {
    socket.on("data", (data) => this.socketData(socket, data));
    socket.on("end", () => this.socketDisconnect(socket));
    socket.on("error", (err) => this.socketError(socket, err));
  }

  socketData(socket, data) {
    const parsedData = this.parseUserInput(data);
    if (!parsedData) return;

    const { message, op, userName ,to} = parsedData;
    const error = this.validateUserMessage(parsedData);
    if (error) return;

    switch (op) {
      case CHANNEL_OPRATOR.CHANGE_NAME:
        this.changeUserName(socket, userName);
        break;
      case CHANNEL_OPRATOR.PRIVATE_MESSAGE:
        this.handlePrivateMessage(socket, message, to);
        break;
      default:
        this.broadcastMessage(socket, message);
        break;
    }
  }

  socketDisconnect(socket) {
    logger.info("Client disconnected.");
    this.usedIds.delete(socket.id);
    this.clients.splice(this.clients.indexOf(socket), 1);
    this.broadcastUserList();
  }

  socketError(socket, err) {
    logger.error(`Client connection error: ${err.message}`);
    this.usedIds.delete(socket.id);
    this.clients.splice(this.clients.indexOf(socket), 1);
    this.broadcastUserList();
  }

  changeUserName(socket, userName) {
    socket.userName = userName;
    this.clients[this.clients.indexOf(socket)] = socket;
    this.broadcastUserList();
  }

  parseUserInput(userInput) {
    try {
      return JSON.parse(userInput);
    } catch (e) {
      socket.write(JSON.stringify({
        error: `Invalid user input format. Please provide input in JSON format: 
        {"message":"your message", "op":"change-name/@"}, 
        userName: "required when user name is changed."`
      }));
      return null;
    }
  }

  validateUserMessage({ message, op, userName,to }) {
    let error = '';
    if (!message) error = 'Message is required';
    if (op === CHANNEL_OPRATOR.CHANGE_NAME && !userName) error = 'UserName is required';
    else if (op === CHANNEL_OPRATOR.PRIVATE_MESSAGE && !to) error = 'To is required';
    if (error) {
      socket.write(JSON.stringify({ error }));
      return error;
    }
    return null;
  }

  handlePrivateMessage(socket, message,to) {
    const recipient = this.clients.find(client => client.userName === to);
    if (recipient) {
      recipient.write(JSON.stringify({
        message,
        messageType: MESSEGE_TYPE.PRIVATE,
        userName: socket.userName || DEFAULT_USER_NAME
      }));
    } else {
      socket.write(JSON.stringify({ error: 'User not found.' }));
    }
  }

  broadcastMessage(socket, message) {
    this.clients.forEach((client) => {
      if (client !== socket) {
        client.write(JSON.stringify({
          messageType: MESSEGE_TYPE.BROADCAST,
          userName: socket?.userName || DEFAULT_USER_NAME,
          message
        }));
      }
    });
  }
}

module.exports = ChatServer;
