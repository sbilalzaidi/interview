require('dotenv').config();
const PORT = process.env.PORT;
const ChatServer = require('./chatServer');
const server = new ChatServer(PORT);
server.start();