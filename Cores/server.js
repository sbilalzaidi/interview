const express = require('express')
const cors = require('cors')
const app = express()
const port=3000;
// const corsOptions = {
//   origin: 'https://example.com',
//   methods: ['GET', 'POST'],
//   allowedHeaders: ['Content-Type', 'Authorization']
// };
//app.use(cors(corsOptions))
app.use(cors())
//custome cores header allow
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");//all origin
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');//method
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); //x request
  next();
});
app.get('/check-cores', function (req, res, next) {
  res.json({msg: 'This is CORS-enabled for all origins!'})
})

app.listen(port, function () {
  console.log('CORS-enabled web server listening on port 80')
})