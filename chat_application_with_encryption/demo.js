const crypto = require('crypto');

// Generate the symmetric key and IV
const symmetricKey = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);

// Convert to strings (e.g., using base64 encoding)
const symmetricKeyStr = symmetricKey.toString('base64');
const ivStr = iv.toString('base64');

console.log('Symmetric Key (String):', symmetricKeyStr);
console.log('IV (String):', ivStr);

// Convert back to Buffers
const symmetricKeyBuffer = Buffer.from(symmetricKeyStr, 'base64');
const ivBuffer = Buffer.from(ivStr, 'base64');

console.log('Symmetric Key (Buffer):', symmetricKeyBuffer);
console.log('IV (Buffer):', ivBuffer);

// Check if the original and converted buffers are the same
console.log('Symmetric Key is the same:', symmetricKey.equals(symmetricKeyBuffer));
console.log('IV is the same:', iv.equals(ivBuffer));

            console.log(symmetricKey.toString())