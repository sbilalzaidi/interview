const net = require('net');
const crypto = require('crypto');
require('dotenv').config(); 
const {CHANNEL_OPRATOR} = require('./constants');
const readline = require('readline');
const logger = require('./logger'); 
const PORT = process.env.PORT;
const Rsa = require('./rsaUtil');
const Aes = require('./aesUtil');

class ChatClient {
  constructor(port,publicKeyPath) {
    this.port = port;
    this.key = crypto.randomBytes(32); // Generate a random key
    this.iv = crypto.randomBytes(16);// Generate a random IV (Initialization Vector)
    this.client = null;
    this.rl = null;
    this.userName = '';
    this.connectToServer();
    this.clientInputHandler();
    this.rsa = new Rsa('./public_key.pem');
    this.aes = new Aes();
  }

  connectToServer() {
    this.client = net.createConnection({ port: this.port }, () => {
      logger.info('CLIENT: I connected to the server.');
      let data=this.rsa.encryptData(JSON.stringify({
        key:this.key.toString('base64'),
        iv:this.iv.toString('base64'),
        message:"my name bilal"
      }))
      this.client.write(data);
    });

    this.client.on('data', (data) => {
      data=this.aes.decryptData(data.toString(),this.key,this.iv)
      this.serverData(data);
    });

    this.client.on('end', () => {
      this.handleServerEnd();
    });
  }

  serverData(data) {
    const parsedData = JSON.parse(data.toString());
    logger.info(`Data received from server: ${JSON.stringify(parsedData)}`);
    this.setUserName(parsedData);
  }

  setUserName(data) {
    if (data?.op === 'change-name' && data?.userName) {
      this.userName = data?.userName;
      logger.info(`User name set to: ${this.userName}`);
    }
  }

  handleServerEnd() {
    logger.info('CLIENT: I disconnected from the server.');
  }

  clientInputHandler() {
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    this.rl.on('line', (userInput) => {
      try {
        const payload = JSON.parse(userInput);
        this.sendMessageToServer({
          ...payload,
          userName: payload?.op !== CHANNEL_OPRATOR.CHANGE_NAME ? this.userName : payload?.userName
        });
      } catch (e) {
        logger.error(`Invalid user input: ${e.message}`);
        logger.info('Invalid user input format. Please provide input in JSON format: {"message":"your message", "op":"change-name/@"}, userName: "required when user name is changed."');
      }
    });
  }

  sendMessageToServer(payload) {
    this.client.write(this.rsa.encryptData(JSON.stringify(payload)));
    logger.info(`Message sent to server: ${JSON.stringify(payload)}`);
  }

  validateUserMessage(payload) {
    if (!payload?.message) {
      throw new Error('Message is required');
    }
    if (payload?.op === CHANNEL_OPRATOR.CHANGE_NAME && !payload?.userName) {
      throw new Error('UserName is required.');
    }
    else if (op === CHANNEL_OPRATOR.PRIVATE_MESSAGE && !to)
      throw new Error('To is required.');
  }
}

new ChatClient(PORT,process.env.CLIENT_PUBLIC_KEY);
