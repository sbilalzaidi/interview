const crypto = require('crypto');

class Aes {
  constructor() {
    this.algorithm = 'aes-256-cbc';
  }

  encryptData(text,key,iv) {
    const cipher = crypto.createCipheriv(this.algorithm, key, iv);
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return encrypted;
  }

  decryptData(encryptedText,key,iv) {
    const decipher = crypto.createDecipheriv(this.algorithm, key, iv);
    let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
  }

  getKey(key) {
    return key.toString('hex');
  }

  getIv(iv) {
    return iv.toString('hex');
  }
}
module.exports = Aes;
// key=crypto.randomBytes(32).toString('base64');
// iv=crypto.randomBytes(16).toString('base64');
// const aes=new Aes()
// console.log(key,"######",iv)
// let en=aes.encryptData("ddddd",Buffer.from(key, 'base64'),Buffer.from(iv, 'base64'))
// console.log(aes.decryptData(en,Buffer.from(key, 'base64'),Buffer.from(iv, 'base64')))