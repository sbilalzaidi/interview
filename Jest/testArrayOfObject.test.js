
const transform=(input)=>{
    let output=[];
    input.forEach(element => {
        element.groupIds.forEach(groupId=>{
            let existingGroup=output.find((group)=>{return group.groupId==groupId})
            if(existingGroup){
                existingGroup.userIds.push(element.userId)
            }else{
                output.push({
                    groupId:groupId,
                    userIds:[element.userId]
                })
            }
        })
    });
    return output;
}
describe('transform',()=>{
    test('transforms input array correctly',()=>{
        const input = [ 
            { userId: 'user0', groupIds: ['groupA', 'groupB', 'groupC'] }, 
            { userId: 'user1', groupIds: ['groupB', 'groupC'] }, 
            { userId: 'user2', groupIds: ['groupA', 'groupB'] }, 
        ]; 
        
        const output = [ 
            { groupId: 'groupA', userIds: ['user0', 'user2'] }, 
            { groupId: 'groupB', userIds: ['user0', 'user1', 'user2'] }, 
            { groupId: 'groupC', userIds: ['user0', 'user1'] }, 
        ];

        expect(transform(input)).toEqual(output);
    });
    test('returns empty array for empty input', () => {
        expect(transform([])).toEqual([]);
    });
})