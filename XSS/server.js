const express=require('express');
const xss=require('xss');

const app=express();
// Middleware to set HTTP headers
app.use((req, res, next) => {
    // Set other headers for security
    res.setHeader('X-XSS-Protection', '1; mode=block'); // XSS protection
    next();
});
//it remove css script and many more
app.get('/',(req,res)=>{
    const userInput="<script>alert('XSS attack!');</script>"

    const sanitizedInput=xss(userInput);
    res.send(`Sanitised input:${sanitizedInput}`);
})
app.listen(3000,()=>{
    console.log('Server is listening on port 3000');
})