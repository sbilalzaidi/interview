const express = require('express'); 
const helmet = require('helmet'); 
const app = express(); 

//with library  
app.use(helmet()); 

//Custome header used
// app.use(function (req, res, next) {
//     res.setHeader(
//       'Content-Security-Policy',
//       "default-src 'self'; font-src 'self'; img-src 'self'; script-src 'self'; style-src 'self'; frame-src 'self'"
//     );
//     next();
//   });

  
app.listen(3000,()=>{
    console.log('Server is listening on port 3000');
})