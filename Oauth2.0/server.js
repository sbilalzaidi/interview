const express = require('express');
const passport = require('passport');
const OAuth2Strategy = require('passport-oauth2').Strategy;
const jwt = require('jsonwebtoken');

const app = express();

// Configure the OAuth 2.0 strategy
passport.use('oauth2', new OAuth2Strategy({
    authorizationURL: 'https://example.com/oauth2/auth',
    tokenURL: 'https://example.com/oauth2/token',
    clientID: 'your-client-id',
    clientSecret: 'your-client-secret',
    callbackURL: 'http://localhost:3000/auth/callback' // Your callback URL
}, (accessToken, refreshToken, profile, done) => {
    // Here you might process the profile or retrieve additional user information
    // For this example, we'll simply create a JWT token with the access token
    const token = jwt.sign({ accessToken }, 'your-secret-key');
    done(null, token);
}));

// Middleware to initialize Passport
app.use(passport.initialize());

// Route to start OAuth 2.0 authentication
app.get('/auth', passport.authenticate('oauth2'));

// Route for OAuth 2.0 callback
app.get('/auth/callback', passport.authenticate('oauth2', {
    session: false,
    successRedirect: '/success',
    failureRedirect: '/failure'
}));

// Route for successful authentication
app.get('/success', (req, res) => {
    res.send('Authentication successful!');
});

// Route for failed authentication
app.get('/failure', (req, res) => {
    res.send('Authentication failed!');
});

// Start the server
app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
