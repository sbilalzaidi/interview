const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const app = express();
const port = 3000;
const secretKey = 'abc123';
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Dummy data
const users = [
  { id: 1, username: 'john_doe', password: '$2a$10$CmiknylHU.ewVJfDKiUgK.1RqJUDAsl6jm99NzEFZzKAAMdkAEFA2', email: 'john@example.com' ,token:''},
  { id: 2, username: 'jane_doe', password: '$2a$10$e1IQfsRHM8UKiM6HoB8fDeRqsuQHjXyTLe38N0vCnk76/GN6vxVZi', email: 'jane@example.com' ,token:''}
];
// Dummy authentication middleware
const authenticateUser = (req, res, next) => {
 let { token } = req.headers;
 // Get the authorization header with the token
 const authHeader = req.headers['authorization'];
 token = authHeader && authHeader.split(' ')[1]; // Bearer <token>

 if (!token) {
   return res.sendStatus(401); // Unauthorized
 }
 jwt.verify(token, secretKey, (err, user) => {
   if (err) {
     return res.sendStatus(403); // Forbidden
   }
   req.user = user;
   next();
 });
  next();
};
//genrate password 
const genratePassword=async(password)=>{
  bcrypt.hash(plaintextPassword, 10, (err, hash) => {
    if (err) {
      console.error(err);
      return null;
    }
    return hash;
  })
}
//validate password
const validatePassword=async(plaintextPassword,hash)=>{
  bcrypt.compare(plaintextPassword, hash, (err, result) => {
    if (err) {
      console.error(err);
      return false;
    }
    console.log('Password Matched:', result);
    return true
  });
}
//validate user
const checkUserNameAndPassword =async (req, res, next) => {
    let { username, password } = req.body;
    const user = users.find(u => u.username === username);
    password=await validatePassword(password,user?.password)
    if (!user || !password) {
      return res.status(401).json({ error: 'Unauthorized' });
    }
    // Find the user in the array
    const userIndex = users.findIndex(u => u.username === username && u.password === password);

  // If the user is found, update the specified key
  if (userIndex !== -1) {
    users[userIndex]['token'] = (new Date()).getTime();
    req.user = user;
    next();
  }
}
// Routes
app.put('/api/users/login', checkUserNameAndPassword,async (req, res) => {
    const {user}=req;
    const accessToken = jwt.sign(user, secretKey,{expiresIn:300});
    res.status(200).json({user,token:accessToken});
});
// 200 OK
app.get('/api/users', authenticateUser, (req, res) => {
  res.status(200).json(users);
});

// 201 Created
app.post('/api/users/register', async(req, res) => {
  let newUser = req.body;
  const userIndex = users.findIndex(u => u.username === newUser.username);
  if(userIndex !== -1)
  res.status(409).json({ message: `User already exists.` });
  else{
    newUser.password=await genratePassword(newUser.password);
    users.push(newUser);
    res.status(201).json(newUser);
  }
});

// 202 Accepted
app.put('/api/users/:id', authenticateUser, (req, res) => {
  const userId = parseInt(req.params.id);
  // Perform some async operation, then respond with 202
  const userIndex = users.findIndex(u => u.id === userId);
  if(userIndex !== -1)
  res.status(202).json({ message: `Update accepted for user ${userId}` });
  else
  res.status(404).json({ message: `User not found` });
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
