Semantic Versioning (often abbreviated as SemVer) is a versioning scheme for software that aims to convey meaning about the nature of changes in a release based on the version number. It consists of three numbers separated by periods: MAJOR.MINOR.PATCH. Here's a breakdown:

MAJOR version: incremented when you make incompatible API changes.
MINOR version: incremented when you add functionality in a backward-compatible manner.
PATCH version: incremented when you make backward-compatible bug fixes.
Let's illustrate with a JavaScript example:

Suppose you have a JavaScript library called "exampleLib" that you want to version using Semantic Versioning.

MAJOR version increment (breaking change):
You make significant changes that break the existing API. For example, you remove a function that many users rely on.
MINOR version increment (feature addition):
You add new functionality to the library in a backward-compatible manner.
PATCH version increment (bug fix):
You fix a bug without changing existing functionality.