// Current version: 2.0.0
function exampleFunction() {
    // Existing implementation
  }
  
  function newFeature() {
    // New functionality
  }
  
  // New version: 2.1.0
  // Feature addition: Adding a new function
  