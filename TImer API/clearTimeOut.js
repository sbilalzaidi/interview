console.log("Before the setTimeout call")
let timerID = setTimeout(() => {console.log("Hello, World!")}, 1000);
console.log("After the setTimeout call")

clearTimeout(timerID)