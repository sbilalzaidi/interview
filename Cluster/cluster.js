const cluster = require('cluster');
const http = require('http');
const os = require('os');
const PORT = 3000;

function startCluster(workerFunction) {
  if (cluster.isMaster) {
    const numCPUs = os.cpus().length;
    console.info("numCPUs:", numCPUs);

    for (let i = 0; i < numCPUs; i++) {
      cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.info(`Worker ${worker.process.pid} died`);
      // You can fork a new worker here if needed
      cluster.fork();
    });
  } else {
    workerFunction();
  }
}

function createServer() {
  const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello, World!\n');
  });
    server.listen(PORT, () => {
    console.log(`Server running at http://localhost:${PORT}/ in worker ${process.pid}`);
  });
}

startCluster(createServer);
