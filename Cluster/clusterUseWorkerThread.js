const express = require('express');
const { Worker, isMainThread, workerData,parentPort } = require('worker_threads');

if (isMainThread) {
  // This code runs in the main thread

  // Create an Express app
  const app = express();
  const port = 3000;

  // Define a route
  app.get('/', (req, res) => {
    // Create a worker thread and pass some data to it
    const worker = new Worker(__filename, { workerData: { message: 'Hello from main thread!' } });

    // Listen for messages from the worker thread
    worker.on('message', (message) => {
      res.send(`Message from worker: ${message}`);
    });
  });

  // Start the server
  app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
  });
} else {
  // This code runs in the worker thread

  // Access the data passed from the main thread
  const { message } = workerData;

  // Perform some work in the worker thread
  const result = `Worker thread received: ${message}`;

  // Send the result back to the main thread
  parentPort.postMessage(result);
}
